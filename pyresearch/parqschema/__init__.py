# Parquet Functions
from collections import OrderedDict
from pyresearch.parqschema.sharedfunctions import *
import pyresearch


docs_dict = OrderedDict()


#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['retSQL'] = {'fargs': 'hiveschema: the returned Hive fields and types returned from parq2hive\ntablename: the name of the table (str)\nlocation: the hdfs location of the table\npartition: any sort of partition string\nstype: The SQL to return (CREATE, DROP, MSCK, SETMSC', 
                       'fret':  'The SQL Statement requested based on the arguments',
                       'shortdesc' : 'A function to return a specific type fo SQL Statement base on arguments',
                       'desc': 'retSQL(hiveschema, tablename, location, partition, stype)\nTo automate various SQL statements, this can take some arguments and return the correct sql statement, including the MSCK statement, CREATE, and DROP statements'
                      }
docs_dict['getparqinfo'] = {'fargs':'in_hdfs_file: The path (hdfs/maprfs) of the parquet file to review',
          'fret': 'The return value the of the parquet tools schema function',
          'shortdesc': 'Get schema info from parqtools for a specific file',
          'desc': 'getpartinfo(in_hdfs_file)\nGet schema info from parqtools'
          }
docs_dict['parq2hive'] = {'fargs':'parqstr: The output from getparqinfo (the parqet tools schema output', \
          'fret': "The field names and hive types for a Hive Create Table Statement",
          'shortdesc': 'Convert parquet tools schema output to Hive CREATE fields',
          'desc': 'parq2hive(parqstr)\nAttempt to recursively create a hive Hive CREATE TABLE statement from the output of a Parquet Tools schema (i.e. the returns from pyresearch.parqschema.getparqinfo'
         }



def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)


